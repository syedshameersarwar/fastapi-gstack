from datetime import datetime
from typing import Any, Dict, Optional

import mangostar.commands as comm
from addict import Addict as DDict
from fastapi import FastAPI, HTTPException
from inflection import tableize
from loguru import logger
from mangostar import commands as comms
from mangostar.circular import DBResponse, _init_tables
from mangostar.settings import ModuleSettings
# from mangostar.mangostar import commands as comm
from pydantic import BaseModel, root_validator
from sqlalchemy import create_engine, text

with logger.catch():
    _init_tables()


class InsertableRecord(BaseModel):
    data: Dict[str, Any]
    event_at: datetime = datetime.now()
    bucket: Optional[str]
    tags: Dict[str, Any] = {}

    @root_validator
    def check_bucket(cls, values: dict):
        doti = DDict(**values)
        buck = doti.bucket
        if buck:
            doti.bucket = tableize(buck)
            return doti.to_dict()

        if "bucket" not in doti.data.keys():
            raise ValueError("Bucket not added.")

        nested_bucket = doti.data.bucket
        if not isinstance(nested_bucket, str):
            raise TypeError(
                "A bucket was found in 'data' but it wasn't the right type.")
        doti.data.pop("bucket", None)

        doti.bucket = str(nested_bucket)

        return doti.to_dict()


class RequestModel(BaseModel):
    data: Dict


with logger.catch():
    records = {
        "data": {
            "bucket": "bakery_inventory",
            "data": {
                "id":
                "0001",
                "type":
                "donut",
                "name":
                "Cake",
                "ppu":
                0.55,
                "batters": {
                    "batter": [{
                        "id": "1001",
                        "type": "Regular"
                    }, {
                        "id": "1002",
                        "type": "Chocolate"
                    }, {
                        "id": "1003",
                        "type": "Blueberry"
                    }, {
                        "id": "1004",
                        "type": "Devil's Food"
                    }]
                },
                "topping": [{
                    "id": "5001",
                    "type": "None"
                }, {
                    "id": "5002",
                    "type": "Glazed"
                }, {
                    "id": "5005",
                    "type": "Sugar"
                }, {
                    "id": "5007",
                    "type": "Powdered Sugar"
                }, {
                    "id": "5006",
                    "type": "Chocolate with Sprinkles"
                }, {
                    "id": "5003",
                    "type": "Chocolate"
                }, {
                    "id": "5004",
                    "type": "Maple"
                }]
            },
        },
        "event_at": "2021-03-19T21:02:09.689606",
        "tags": {
            "store_name": "shoeberry bakery",
            "city": "los angeles",
            "state": "california",
            "id": "e662377992144ebb823628a7cb32ca6e"
        }
    }
    req = RequestModel(data=records)

    record_fitted = InsertableRecord(**req.data)
    # print(record_fitted.dict())
    comm.insert_dict(**record_fitted.dict())
# psql = "postgresql://postgres:8HykTNVQDv@postgresql.default.svc.cluster.local:5432/postgres"
# # postgresql://postgres:8HykTNVQDv@postgresql.default.svc.cluster.local:5432/postgres
# # postgresql://postgres:8HykTNVQDv@postgresql.default.svc.cluster.local:5432/postgres
# sets = ModuleSettings()
# e = create_engine(sets.postgres.connstr)

# print(sets.postgres.connstr)
# with e.connect() as conn:
#     cursor = conn.execute(text("SELECT * FROM information_schema.tables"))
#     print(list(cursor)[0])
