#!/bin/bash

export NUM_CORES = $(nproc --all)
pip install -r requirements.txt --install-option="--jobs=$NUM_CORES"