if [ ! -z "${ACCESS_TOKEN}" ]; then
    poetry config http-basic.monorepo_packages __token__ ${ACCESS_TOKEN}
else
    echo "No ACCESS_TOKEN env variable is present."
fi

if [ ! -z "${SHAVED_URL}" ]; then
    poetry config repositories.monorepo_packages "https://${SHAVED_URL}"
else
    echo "No SHAVED_URL env variable is present."
fi 
