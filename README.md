# OpenFaas Python3 FastAPI Template

A Python [FastAPI](https://github.com/tiangolo/fastapi) template that make use of the incubator project [of-watchdog](https://github.com/openfaas-incubator/of-watchdog). It has a bunch of bells and whistles to it to give it both a faster response time and allow for internal repos to work.

Templates available in this repository:
- python3-fastpoet


## Downloading the templates
```
$ faas-cli template pull https://gitlab.com/kivo360/python-fastapi-poetry-template.git
```

## Using the python3-fastinit templates
Create a new function

```
$ faas-cli new --lang python3-fastinit <fn-name>
```

Build, push, and deploy

```
$ faas-cli up -f <fn-name>.yml
```

Set your OpenFaaS gateway URL. For example:

```
$ OPENFAAS_URL=http://127.0.0.1:8080
```

Test the new function

```
$ curl -i $OPENFAAS_URL/function/<fn-name>
```

## Usage

### Request Body
The function handler is passed one argument - *req* which contains the request body.

### Response Bodies
By default, the template will automatically attempt to set the correct Content-Type header for you based on the type of response.


```bash
export DOCKER_USERNAME=admin
export DOCKER_PASSWORD=gtcd2sbMhFUG8zQDrGxU
export DOCKER_EMAIL=kah.kevin.hill@gmail.com
export DOCKER_SERVER=registry.gitlab.com
```

```bash
kubectl create secret docker-registry monorepo-registry \
    --docker-username=$DOCKER_USERNAME \
    --docker-password=$DOCKER_PASSWORD \
    --docker-email=$DOCKER_EMAIL \
    --docker-server=$DOCKER_SERVER
    --namespace openfaas-fn
```
<!-- groma1/bodhi-app/monorepo -->

<!-- docker build -t registry.gitlab.com/kivo360/python-fastapi-poetry-template . -->